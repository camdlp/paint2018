/*
 * Esta clase dibuja Pentágonos (o eso creo)
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author carlosabia
 */
public class Pentagono extends Forma {

    public Pentagono(int _posX, int _posY, Color _color, boolean _relleno) {
        //inicializa el constructor del pentágono correctamente para que guarde 5 lados.
        super(_posX, _posY, 5, _color, _relleno);
        
    }

}
