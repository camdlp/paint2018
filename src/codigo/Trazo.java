/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.BasicStroke;

/**
 *
 * @author carlosabia
 */
public class Trazo extends BasicStroke{
    public Trazo(int _grosor){
        super(_grosor);
    }
    
    public Trazo(int _grosor, boolean _rayas){
        super(_grosor, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f, new float[]{10.0f}, 0.0f);
    }
}
