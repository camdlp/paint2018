/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

/**
 *
 * @author carlosabia
 */
public class Linea extends Line2D.Double{ 
    public Color color = null;
    public boolean relleno = false;
    //coordenadas del centro de la forma
    public int x1;
    public int y1;
    public int x2;
    public int y2;
    public Linea(int _posX1, int _posY1, int _posX2, int _posY2, Color _color, boolean _relleno){
        super();
        color = _color;
        relleno = _relleno;
        this.x1 = _posX1;
        this.y1 = _posY1;
        this.x2 = _posX2;
        this.y2 = _posY2;
    }
    
    public void dibujate(Graphics2D g2, int _posX1, int _posY1, int _posX2, int _posY2, BasicStroke _trazo){
        g2.setColor(color);
        g2.setStroke(_trazo);
        g2.drawLine(_posX1, _posY1, _posX2, _posY2);
        
    }
    
}
