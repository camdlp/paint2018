/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

/**
 *
 * @author carlosabia
 */
public class Libre extends Line2D.Double {

    public boolean relleno = false;
    public int x1;
    public int y1;
    public int x2;
    public int y2;
    public Color color = null;

    public Libre(int _posX1, int _posY1, int _posX2, int _posY2, Color _color, boolean _relleno) {
        super();
        color = _color;
        relleno = _relleno;
        this.x1 = _posX1;
        this.y1 = _posY1;
        this.x2 = _posX2;
        this.y2 = _posY2;
    }

    public void dibujate(Graphics2D g2, int _posX1, int _posY1, int _posX2, int _posY2, BasicStroke _trazo) {
        g2.setColor(color);
        g2.setStroke(_trazo);
        g2.drawLine(x1, y1, x2, y2);
        x1 = x2;
        y1 = y2;
        

    }
}
